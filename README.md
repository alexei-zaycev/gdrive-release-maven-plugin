# Google Drive Maven Wagon
This project is a [Maven Wagon](http://maven.apache.org/wagon) for the Google Drive API v3. 

An OAuth 2.0 authorization is required to provide consent for the Google registered Rogueware-GoogleDriveMavenPlugin application.

This ensures a registered application is referenced by the Wagon when managing artefacts in a Google Drive acting as a Maven repository.

## Features
* Designate a folder in Google Drive as a Maven repository
* Publish to Google Drive as a Maven repository
* Use a Google Drive as a Maven dependency repository
* Share the repository with other Google Drive users


## Authorization
A single MOJO task is provided to authorise with Google Drive and generate a server entry for the `settings.xml` file. 

The interactive task will prompt for the desired repository role and redirect to the Google consent screen for authorization. The xml for the server entry will be generated and optionally written to the local `settings.xml` file. 

```
mvn org.rogueware.mojo:gdrive-release-maven-plugin:authorize
```

A server entry with the default server id **gdrive** is generated. The server entry contains the stored credential containing the authorised access token and selected role for an authorised Google account.

Use the server.id parameter to specify the server id

```
mvn org.rogueware.mojo:gdrive-release-maven-plugin:authorize -Dserver.id=gdrive-personnel
```

### Repository Role
One of three repository roles may be selected during authorisation.

* **PUBLISHER**: 

	This provides the most flexible team shared repository having developers read from and publish to the repository. 

	A single team member will own the folder on their Google drive and share it `Can organize, add & edit` with other team members who are publishers.
	
* **SUBSCRIBER**

	This provides a read only view of a repository shared by a team. The owner will share the folder `Can view only` with team members who are subscribers.

* **PRIVATE**

	Used as a personnel repository that cannot be shared. The repository will be stored in the application space of the Google application providing the most restrictive access to the Google drive of the user.

### Stored Credential Portability
The server entry may be copied and added to `settings.xml` files located on different machines or servers.

**Warning:** The server entry contains a stored credential representing the authorisation token for a specified Google account. Like all server credentials it is recommended that the entry is kept private and local to the developers local `settings.xml` file. 

Standard Google drive folder sharing should be used to allow multiple developers with their own stored credentials access to a single Maven repository.


## Usage
One or more server entries in the `settings.xml` file are required via the authorization process.

### Extension
A build extension must be defined in a project's `pom.xml` to enable publishing Maven artifacts to a Google Drive.  

```xml
<project>
  ...
  <build>
    <extensions>
      <extension>
        <groupId>org.rogueware.mojo</groupId>
        <artifactId>gdrive-release-maven-plugin</artifactId>
      </extension>
      ...
    </extensions>
    ...
  </build>
  ...
</project>
```

The extension will enable the `gdrive:` schema that can be used in repository and distribution management.

The path in the `gdrive:[path]` URI references the folder on the Google Drive resolved firstly looking under the `Shared with me` and then trying `My Drive`. The path will automatically be created under `My Drive` if it does not exist when accessing the repository as a publisher role or within the application private space when accessing the repository in the private role.

### Distribution Management

Distribution management repositories can be defined in the `pom.xml` with the `gdrive:/` scheme.

Only an account authorized with the publisher or private repository roles are able to publish maven artefacts to repositories.

```xml
<project>
  ...
  <distributionManagement>
    <repository>
      <id>gdrive</id>
      <name>Google Drive Release Repository</name>
      <url>gdrive:/maven-artifacts/release</url>
    </repository>
    
    <snapshotRepository>
      <id>gdrive-snapshot</id>
      <name>Google Drive Snapshot Repository</name>
      <url>gdrive:/maven-artifacts/snapshot</url>
    </snapshotRepository>
  </distributionManagement>
  ...
</project>
```

The <id> refers to the server id created in the `settings.xml` during the authorization process.

 
### Repositories
Publishers and subscribers are able to reference artefacts published in a Google Drive Maven repository.

Repositories are defined in the `pom.xml` with the `gdrive:` scheme.

```xml
<project>
  ...
  <repositories>
    ...
    <repository>
      <id>gdrive</id>
      <url>gdrive:/maven-artifacts/release</url>
    </repository>
    
    <repository>
      <id>gdrive-snapshot</id>
      <url>gdrive:/maven-artifacts/snapshot</url>
    </repository>
   </repositories>  
   ...
</project>
```


### Snapshots
Snapshots are supported by the Maven Wagon.

The owner of the repository will need to manually remove unused historical snapshot releases as they will acccumulate on the Google drive.


## Sharing Repositories
The owner is able to share the folder with other Google Drive users to allow repository sharing.

Share the folder either directly with a user's Google user or add the user's to a [Google Group](http://groups.google.com) or [G Suite](http://apps.google.com) group that has been granted access to the folder.

Sharing the folder with `Can organize, add & edit` with allow other Google Driver users the repository publisher role.
 
Sharing the folder with `Can view only` with allow other Google Driver users the repository subscriber role.


## Debug
Add `<debug>true</debug>` to the server configuration section in the `settings.xml` file.

``` 
    <server>
      <id>gdrive</id>
      <configuration>
        <debug>true</debug>
        <storedCredential>...</storedCredential>
      </configuration>
    </server>
```