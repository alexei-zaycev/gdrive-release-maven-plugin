/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.mojo;

import org.rogueware.maven.SettingsUtils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.Base64DataStoreFactory;
import com.google.api.client.util.store.RepositoryRole;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.components.interactivity.Prompter;
import org.codehaus.plexus.components.interactivity.PrompterException;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.rogueware.maven.ClientSecret;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Mojo(name = "authorize", threadSafe = true, requiresProject = false)
public class AuthorizeMojo extends AbstractMojo {
   // Execute Goal: mvn org.rogueware.mojo:gdrive-release-maven-plugin:authorize
   //    server Id: mvn org.rogueware.mojo:gdrive-release-maven-plugin:authorize -Dserver.id=gdrive-personnel

   @Component
   private Prompter prompter;

   @Parameter(defaultValue = "${settings}", readonly = true)
   private Settings settings;

   @Parameter(readonly = true, required = true, defaultValue = "${user.home}")
   private String userHome;

   @Parameter(property = "server.id", defaultValue = "gdrive")
   private String serverId;

   @Override
   public void execute() throws MojoExecutionException, MojoFailureException {
      try {
         RepositoryRole role = promptForRole();
         String storedCredential = authorize(role);
         saveStoredCredential(storedCredential);
      } catch (Exception ex) {
         getLog().error(String.format("Unable to authorise Google API project Rogueware-GoogleDriveMavenPlugin/1.0 using Maven server id %s, error: %s",
                 serverId, ex.getMessage()));
         throw new MojoExecutionException(ex.getMessage(), ex);
      }
   }

   private RepositoryRole promptForRole() throws Exception {
      String r = prompter.prompt("Select the repository role:\r\n"
              + "   Repository (P)ublisher: Read-Write access to owned or shared repository\r\n"
              + "   Repository (S)ubscriber: Read-Only access to shared repository\r\n"
              + "   Repository P(R)ivate: Single user repository\r\n");

      switch (r.toUpperCase()) {
         case "S":
            return RepositoryRole.REPOSITORY_SUBSCRIBER;

         case "P":
            return RepositoryRole.REPOSITORY_PUBLISHER;

         case "R":
            return RepositoryRole.REPOSITORY_PRIVATE;
      }
      throw new Exception("Please select a valid role (P, S, R)");
   }

   private String authorize(RepositoryRole role) throws Exception {
      getLog().info(String.format("Authorising Google API project Rogueware-GoogleDriveMavenPlugin/1.0 using Maven server id %s and role %s",
              serverId, role));

      JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
      HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      Base64DataStoreFactory dataStoreFactory = new Base64DataStoreFactory(role);

      // Load client secret
      GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory,
              new InputStreamReader(ClientSecret.getClientSecretInputStream(AuthorizeMojo.class.getResourceAsStream("/client/client_secrets.dat"))));
      
      // set up authorization code flow (Role determines the drive access scope)
      GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
              httpTransport, jsonFactory, clientSecrets,
              Collections.singleton(role.getDriveScope())).setDataStoreFactory(dataStoreFactory)
              .build();
      // authorize
      Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

      return dataStoreFactory.getStoredCredentialString();
   }

   private void saveStoredCredential(String storedCredential) throws PrompterException, IOException, FileNotFoundException, XmlPullParserException {
      // Output the server entry
      getLog().info("Authorized ... the following server entry represents your Google Drive");

      StringBuilder sb = new StringBuilder();
      sb.append("      <server>\n");
      sb.append("         <id>");
      sb.append(serverId);
      sb.append("</id>\n");
      sb.append("         <configuration>\n");
      sb.append("            <timeout>90000</timeout>\n");  // Defailt to 1m30s connection timeout 
      sb.append("            <readTimeout>300000</readTimeout>\n");  // Defailt to 5m read timeout 
      sb.append("            <storedCredential>");
      sb.append(storedCredential);
      sb.append("</storedCredential>\n");
      sb.append("         </configuration>\n");
      sb.append("      </server>\n");
      System.out.print(sb.toString());

      java.io.File settingsFile = new java.io.File(userHome + "/.m2/settings.xml");
      if (!settingsFile.exists() || !settingsFile.isFile() || !settingsFile.canWrite()) {
         getLog().error(String.format("User settings file %s is not accessible", settingsFile.getAbsolutePath()));
         return;
      }

      String yn;
      if (null == SettingsUtils.getServer(settings, serverId)) {
         yn = prompter.prompt(String.format("Write server entry for server id %s to settings.xml (y/n)?", serverId));
      } else {
         String existingStoredCredential = SettingsUtils.getServerStoredClientCredential(settings, serverId);
         yn = null == existingStoredCredential
                 ? prompter.prompt(String.format("An existing standard server entry for server id %s exists in settings.xml. Replace with Google Drive entry (y/n)?", serverId))
                 : prompter.prompt(String.format("An existing server entry for server id %s already exists in settings.xml. Overwrite (y/n)?", serverId));
      }

      if (!"y".equals(yn)) {
         getLog().warn(String.format("Not writing server entry for server id %s to settings.xml", serverId));
         return;
      }

      // Store credential in settings.xml
      SettingsUtils.addServerToSettingsFile(settingsFile, serverId, storedCredential);
      getLog().info(String.format("Added server id %s entry to settings file %s", serverId, settingsFile.getAbsolutePath()));
   }
}
