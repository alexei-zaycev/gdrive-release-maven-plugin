/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven;

import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.rogueware.maven.wagon.providers.GoogleDrive;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ClientSecret {
   // http://aes.online-domain-tools.com

   public static final CipherInputStream getClientSecretInputStream(InputStream is) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
      Key secretKeySpec = new SecretKeySpec(getAESKey(), "AES");

      Cipher decryptCipher = Cipher.getInstance("AES/CBC/NoPadding");
      IvParameterSpec ivParameterSpec = new IvParameterSpec(new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
      decryptCipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

      CipherInputStream cipherInputStream = new CipherInputStream(is, decryptCipher);
      return cipherInputStream;
   }

   private static byte[] getAESKey() {
      String aesKey = new Scanner(GoogleDrive.class.getResourceAsStream("/client/aes.dat"), "UTF-8").useDelimiter("\\A").next();
      byte[] aesKeyBytes = hexStringToByteArray(aesKey);
      return aesKeyBytes;
   }

   private static byte[] hexStringToByteArray(String s) {
      int len = s.length();
      byte[] data = new byte[len / 2];
      for (int i = 0; i < len; i += 2) {
         data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                 + Character.digit(s.charAt(i + 1), 16));
      }
      return data;
   }
}
