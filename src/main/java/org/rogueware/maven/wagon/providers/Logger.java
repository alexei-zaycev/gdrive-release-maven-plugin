/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven.wagon.providers;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class Logger {

   private static boolean debug;
   private static boolean trace;

   public static void setLogLevels(boolean enableDebug, boolean enableTrace) {
      debug = enableDebug;
      trace = enableTrace;
   }

   public static void debug(String msg) {
      if (debug) {
         String log = String.format("[DEBUG]  Google Drive Wagon - %s", msg);
         System.out.println(log);
      }
   }

   public static void debugStart(String msg) {
      if (debug) {
         String log = String.format("[DEBUG]  Google Drive Wagon - %s", msg);
         System.out.print(log);
      }
   }

   public static void debugAppend(String msg) {
      if (debug) {
         System.out.print(msg);
      }
   }

   public static void debugEnd() {
      if (debug) {
         System.out.println();
      }
   }

   public static void trace(String msg) {
      if (trace) {
         String log = String.format("[TRACE]  Google Drive Wagon - %s", msg);
         System.out.println(log);
      }
   }

   public static void traceStart(String msg) {
      if (trace) {
         String log = String.format("[TRACE]  Google Drive Wagon - %s", msg);
         System.out.print(log);
      }
   }

   public static void traceAppend(String msg) {
      if (trace) {
         System.out.print(msg);
      }
   }

   public static void traceEnd() {
      if (trace) {
         System.out.println();
      }
   }

}
