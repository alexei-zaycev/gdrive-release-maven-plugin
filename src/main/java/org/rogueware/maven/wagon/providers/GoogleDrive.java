/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven.wagon.providers;

import org.rogueware.maven.ClientSecret;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.MavenSettingsDataStoreFactory;
import com.google.api.client.util.store.RepositoryRole;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import org.rogueware.mojo.AuthorizeMojo;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class GoogleDrive {
   // https://developers.google.com/drive/v3/web/about-sdk
   // https://developers.google.com/api-client-library/java/apis/drive/v3

   // https://console.developers.google.com/cloud-resource-manager
   private static final String APPLICATION_NAME = "Rogueware-GoogleDriveMavenPlugin/1.0";

   private final int connectionTimeout;
   private final int readTimeout;

   private HttpTransport httpTransport;
   private Drive drive;
   private RepositoryRole role;

   public GoogleDrive(int connectionTimeout, int readTimeout) {
      this.connectionTimeout = connectionTimeout;
      this.readTimeout = readTimeout;
   }

   public RepositoryRole getRole() {
      return role;
   }

   public void connect(String serverId, String storedCredential) throws IOException, GeneralSecurityException {
      // Authorize
      httpTransport = GoogleNetHttpTransport.newTrustedTransport();

      JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
      MavenSettingsDataStoreFactory dataStoreFactory = new MavenSettingsDataStoreFactory(serverId, storedCredential);

      // Load client secret
      GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory,
              new InputStreamReader(ClientSecret.getClientSecretInputStream(AuthorizeMojo.class.getResourceAsStream("/client/client_secrets.dat"))));

      // set up authorization code flow
      GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
              httpTransport, jsonFactory, clientSecrets,
              Collections.singleton(dataStoreFactory.getRole().getDriveScope())).setDataStoreFactory(dataStoreFactory)
              .build();
      // authorize
      Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
      role = dataStoreFactory.getRole();
      drive = new Drive.Builder(httpTransport, jsonFactory, setHttpTimeout(credential)).setApplicationName(APPLICATION_NAME).build();
   }

   public void disconnect() {
      if (null != httpTransport) {
         try {
            httpTransport.shutdown();
         } catch (Exception ex) {
            // :(
         }
         httpTransport = null;
      }
   }

   public File downloadFile(File downloadGoogleDriveFile, OutputStream outputStream) throws IOException {
      Drive.Files.Get request = drive.files().get(downloadGoogleDriveFile.getId());

      request.getMediaHttpDownloader().setDirectDownloadEnabled(true);
      // Our listener wraps the output stream to allow intercepting the bytes and updating underlying implementation correctly
      long start = System.currentTimeMillis();
      try {
         request.executeMediaAndDownloadTo(outputStream);
      } catch (NullPointerException ex) {
         // Direct download sometimes throws null pointer as internally getContentLength does not work
         // Just ignore
      } finally {
         outputStream.flush();
         Logger.debug(String.format("Downloaded file (%dms)%s[id=%s] from Google Drive",
                 (System.currentTimeMillis() - start), downloadGoogleDriveFile.getName(), downloadGoogleDriveFile.getId()));
      }

      return downloadGoogleDriveFile;
   }

   public File uploadFile(String parentId, String name, String mimeType, InputStream contentStream) throws IOException {
      // Upload file data
      List<File> existingFile = searchForFileInFolder(parentId, null, name);

      InputStreamContent mediaContent = new InputStreamContent(mimeType, contentStream);
      if (contentStream instanceof MavenGoogleDriveUploadFeedbackInputStream) {
         mediaContent.setLength(((MavenGoogleDriveUploadFeedbackInputStream) contentStream).getSize());
      }

      long start = System.currentTimeMillis();
      try {
         if (existingFile.isEmpty()) {
            // Create the file
            File fileMetadata = new File();
            fileMetadata.setName(name);
            if (null != parentId) {
               fileMetadata.setParents(Arrays.asList(new String[]{parentId}));
            }

            Drive.Files.Create request = drive.files().create(fileMetadata, mediaContent);
            request.setFields("id");
            File file = request.execute();
            return file;
         } else {
            // Update the content of the existing file
            Drive.Files.Update request = drive.files().update(existingFile.get(0).getId(), null, mediaContent);
            File file = request.execute();
            return file;
         }
      } finally {
         if (contentStream instanceof MavenGoogleDriveUploadFeedbackInputStream) {
            ((MavenGoogleDriveUploadFeedbackInputStream) contentStream).finish();
         }
         if (existingFile.isEmpty()) {
            Logger.debug(String.format("Uploaded file (%dms)%s to Google Drive parent[id=%s] ",
                    (System.currentTimeMillis() - start), name, parentId));
         } else {
            Logger.debug(String.format("Updated existing file (%dms)%s on Google Drive parent[id=%s] ",
                    (System.currentTimeMillis() - start), name, parentId));
         }
      }
   }

   public File getSharedWithMeFolder(String folderName, boolean enforceWritable) throws IOException {
      folderName = normaliseFolderName(folderName);

      // Make sure the top folder is shared and has the correct editable status
      String topFolder = folderName.split("/")[0];
      folderName = folderName.substring(topFolder.length());

      List<File> res = searchForFileInFolder(null, "application/vnd.google-apps.folder", topFolder, true);
      if (res.isEmpty()) {
         return null;
      }

      // Check to make sure the read / write permissions are correct on the share
      File shared = res.get(0);
      if (enforceWritable && !shared.getCapabilities().getCanEdit()) {
         throw new IOException(String.format("Google drive folder is required to be writable, but is read-only. Authorize as repository role REPOSITORY_PUBLISHER"));
      } else if (!enforceWritable && shared.getCapabilities().getCanEdit()) {
         throw new IOException(String.format("Google drive folder is required to be read-only, but is writable. Authorize as repository role REPOSITORY_SUBSCRIBER"));
      }

      if (!folderName.isEmpty()) {
         if (role == RepositoryRole.REPOSITORY_SUBSCRIBER) {
            // Subscriber can create folders under the shared folder to get to the base folder id
            shared = createFolder(shared.getId(), folderName);
         } else {
            // Publisher requires the folder to already exist under the shared folder
            shared = searchForFile(shared.getId(), "application/vnd.google-apps.folder", folderName);
         }
      }

      return shared;
   }

   public File createRootFolder(String folderName) throws IOException {
      return createFolder(null, folderName);
   }

   public File createFolder(String parentId, String folderName) throws IOException {
      folderName = normaliseFolderName(folderName);

      String topFolder = folderName.split("/")[0];
      folderName = folderName.substring(topFolder.length());

      List<File> files = searchForFileInFolder(parentId, "application/vnd.google-apps.folder", topFolder);
      File file = files.isEmpty() ? null : files.get(0);
      if (null == file) {
         long start = System.currentTimeMillis();

         File fileMetadata = new File();
         fileMetadata.setName(topFolder);
         fileMetadata.setMimeType("application/vnd.google-apps.folder");
         if (null != parentId) {
            fileMetadata.setParents(Arrays.asList(new String[]{parentId}));
         }

         file = drive.files().create(fileMetadata)
                 .setFields("id")
                 .execute();
//         Logger.traceAppend(String.format("(%dms)%s ", (System.currentTimeMillis() - start), topFolder));
      } else {
//         Logger.traceAppend(String.format("!%s ", topFolder));
      }

      if (!folderName.isEmpty()) {
         file = createFolder(file.getId(), folderName);
      }

      return file;
   }

   public File searchForFile(String parentId, String path) throws IOException {
      Logger.traceStart(String.format("Searching for file under parent[id=%s] %s", parentId, path));

      File res = searchForFile(parentId, null, path);

      Logger.traceEnd();
      return res;
   }

   private File searchForFile(String parentId, String mimeType, String path) throws IOException {
      path = normaliseFolderName(path);

      String top = path.split("/")[0];
      path = path.substring(top.length());
      List<File> files = searchForFileInFolder(parentId, mimeType, top);

      if (files.isEmpty()) {
         return null;  // Element in path not found
      }

      if (path.isEmpty()) {
         // Found what we are looking for
         return files.get(0);
      } else {
         return searchForFile(files.get(0).getId(), path);
      }
   }

   public List<File> searchForFileInRoot(String parentId, String fileWildcard) throws IOException {
      return searchForFileInFolder(parentId, null, fileWildcard);
   }

   public List<File> searchForFile(String fileWildcard) throws IOException {
      return searchForFileInFolder(null, null, fileWildcard, false);
   }

   public List<File> searchForFileInFolder(String parentId, String mimeType, String fileWildcard) throws IOException {
      return searchForFileInFolder(parentId, mimeType, fileWildcard, false);
   }

   private List<File> searchForFileInFolder(String parentId, String mimeType, String fileWildcard, boolean sharedWithMe) throws IOException {
      // Prefix matching or exact name matching
      //System.out.println("Search: " + parentId + ":" + fileWildcard);
      String q = "not trashed";

      if (sharedWithMe) {
         // Subfolders of a shared folder don't have the sharedWithMe flag set!!
         q += " and sharedWithMe";
      }

      if (fileWildcard.endsWith("*")) {
         q += " and name contains '" + fileWildcard.substring(0, fileWildcard.length() - 1) + "'";
      } else {
         q += " and name='" + fileWildcard + "'";
      }

      if (null != mimeType) {
         q += " and mimeType='" + mimeType + "'";
      }
      if (null != parentId) {
         q += " and '" + parentId + "' in parents";
      }

      long start = System.currentTimeMillis();
      Logger.traceStart(String.format("Searching for file in folder %s", q));

      FileList result = drive.files().list()
              .setQ(q)
              .setSpaces("drive")
              .setFields("files(id, name, size, modifiedTime, capabilities, parents)")
              .execute();

      Logger.traceAppend(String.format(" (%dms)[size=%d]", (System.currentTimeMillis() - start), result.getFiles().size()));
      for (File f : result.getFiles()) {
         Logger.traceAppend(String.format(" %s[id=%s]", f.getName(), f.getId()));
      }
      Logger.traceEnd();
      return result.getFiles().isEmpty() ? Collections.EMPTY_LIST : result.getFiles();
   }

   public void deleteFiles(String parentId, String fileWildcard) throws IOException {
      List<File> files = searchForFileInFolder(parentId, null, fileWildcard);
      long start = System.currentTimeMillis();

      if (!files.isEmpty()) {
         Logger.traceStart("Deleting file(s)");
         for (File f : files) {
            drive.files()
                    .delete(f.getId())
                    .execute();
            Logger.traceAppend(String.format(" (%dms)%s", (System.currentTimeMillis() - start), f.getName()));
         }
         Logger.traceEnd();
      }
   }

   public void trashFiles(String parentId, String fileWildcard) throws IOException {
      List<File> files = searchForFileInFolder(parentId, null, fileWildcard);
      long start = System.currentTimeMillis();

      if (!files.isEmpty()) {
         Logger.traceStart("Moving file(s) to trash");
         for (File f : files) {
            File trashFile = new File();
            trashFile.setId(f.getId());
            trashFile.setTrashed(true);
            drive.files()
                    .update(parentId, trashFile)
                    .execute();
            Logger.traceAppend(String.format(" (%dms)%s", (System.currentTimeMillis() - start), f.getName()));
         }
         Logger.traceEnd();
      }
   }

   public String getMineType(String fileName) {
      if (!fileName.contains(".")) {
         return "application/octet-stream";
      }

      String ext = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
      switch (ext) {
         case "bz2":
            return "application/x-bzip2";
         case "jar":
            return "application/java-archive";
         case "war":
         case "ear":
            return "application/x-zip";
         case "zip":
            return "application/zip";
         case "md5":
         case "sha1":
            return "text/plain";
         default:
            return "application/octet-stream";
      }
   }

   private HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
      return new HttpRequestInitializer() {
         @Override
         public void initialize(HttpRequest httpRequest) throws IOException {
            requestInitializer.initialize(httpRequest);
            httpRequest.setConnectTimeout(connectionTimeout); // ms
            httpRequest.setReadTimeout(readTimeout); // ms
         }
      };
   }

   private String normaliseFolderName(String folderName) {
      if (folderName.startsWith("/")) {
         folderName = folderName.substring(1);
      }
      if (folderName.endsWith("/")) {
         folderName = folderName.substring(0, folderName.length() - 1);
      }
      return folderName;
   }
}
