/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven.wagon.providers;

import com.google.api.client.util.store.RepositoryRole;
import com.google.api.services.drive.model.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.InputData;
import org.apache.maven.wagon.OutputData;
import org.apache.maven.wagon.PathUtils;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.StreamWagon;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.events.TransferEvent;
import org.apache.maven.wagon.resource.Resource;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class GoogleDriveWagon extends StreamWagon {

   // Configuration under <configuration> for server in settings.xml
   private String storedCredential;
   private boolean debug;
   private boolean trace;

   // Internal
   private String gDriveBaseDir;
   private File gDriveBaseDirFile;

   private GoogleDrive googleDrive;

   @Override
   public boolean resourceExists(String resourceName)
           throws TransferFailedException, AuthorizationException {

      String filePath = resourceName;
      String[] fileparts = filePath.split(java.io.File.separator);

      try {
         File downloadGoogleDriveFile = null;

         List<File> files = googleDrive.searchForFile(fileparts[fileparts.length - 1]);
         if (files.size() > 1) {
            // Not sure which one to use ... search under specific path
            downloadGoogleDriveFile = googleDrive.searchForFile(gDriveBaseDirFile.getId(), filePath);
         } else if (files.size() == 1) {
            downloadGoogleDriveFile = files.get(0);
         }

         if (null == downloadGoogleDriveFile) {
            Logger.debug(String.format("Google Drive file does not exist: %s", filePath));
            return false;
         } else {
            Logger.debug(String.format("Google Drive file exists: %s", filePath));
            return true;
         }
      } catch (IOException ex) {
         throw new TransferFailedException("Could check if Google Drive file exists: " + gDriveBaseDirFile + "/" + filePath, ex);
      }
   }

   @Override
   public void fillInputData(InputData inputData) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
      // Confirm download from GDrive and override to download using GDrive correctly
      // --------------------

      Resource resource = inputData.getResource();
      String filePath = resource.getName();
      String[] fileparts = filePath.split(java.io.File.separator);
      //    boolean isSnapshotFile = fileparts[fileparts.length - 1].contains("SNAPSHOT");

      Logger.debugStart(String.format("Downloading Google Drive file %s[", filePath));
      try {
         File downloadGoogleDriveFile = null;
         /*      if (isSnapshotFile) {
            // SNAPSHOT (When no maven-metadata.xml present with snapshot details)
            // https://cwiki.apache.org/confluence/display/MAVENOLD/Repository+-+SNAPSHOT+Handling
            String fileWildcard = fileparts[fileparts.length - 1];
            fileWildcard = fileWildcard.split("SNAPSHOT")[0] + "*";

            String fileExt = fileparts[fileparts.length - 1];
            fileExt = fileExt.split("SNAPSHOT")[1];

            // Find the latest SNAPSHOT file matching our file extension
            List<File> files = googleDrive.searchForFile(fileWildcard);
            for (File f : files) {
               if (f.getName().endsWith(fileExt)) {
                  if (null == downloadGoogleDriveFile || f.getName().compareTo(downloadGoogleDriveFile.getName()) > 0) {
                     downloadGoogleDriveFile = f;
                  }
               }
            }
         } else {*/
         // Release
         List<File> files = googleDrive.searchForFile(fileparts[fileparts.length - 1]);

         if (files.size() > 1) {
            // Not sure which one to use ... search under specific path
            downloadGoogleDriveFile = googleDrive.searchForFile(gDriveBaseDirFile.getId(), filePath);
         } else if (files.size() == 1) {
            downloadGoogleDriveFile = files.get(0);
         }
         //     }

         if (null == downloadGoogleDriveFile) {
            throw new ResourceDoesNotExistException("Google Drive file: " + gDriveBaseDirFile + "/" + filePath + " does not exist");
         }

         if (null == downloadGoogleDriveFile.getSize() || null == downloadGoogleDriveFile.getModifiedTime()) {
            throw new IOException("Unable to determine Google drive file size or modified time");
         }

         Logger.debugAppend(String.format("id=%s,size=%d,modified=%d]",
                 downloadGoogleDriveFile.getId(), downloadGoogleDriveFile.getSize(), downloadGoogleDriveFile.getModifiedTime().getValue()));

         resource.setContentLength(downloadGoogleDriveFile.getSize());
         resource.setLastModified(downloadGoogleDriveFile.getModifiedTime().getValue());

         // Setup place holder input stream to make abstract wagon happy and pass in google file until we take over the transfer method
         inputData.setInputStream(new PlaceHolderInputStream(new byte[0], downloadGoogleDriveFile));
      } catch (IOException ex) {
         throw new TransferFailedException("Could not read from Google Drive file: " + gDriveBaseDirFile + "/" + filePath, ex);
      } finally {
         Logger.debugEnd();
      }
   }

   @Override
   protected void transfer(Resource resource, InputStream input, OutputStream output, int requestType, long maxSize) throws IOException {
      // Override to download using GDrive correctly
      // --------------------

      // Download
      File downloadGoogleDriveFile = ((PlaceHolderInputStream) input).getGoogleFile();
      try {
         TransferEvent transferEvent = new TransferEvent(this, resource, TransferEvent.TRANSFER_PROGRESS, TransferEvent.REQUEST_GET);
         transferEvent.setTimestamp(System.currentTimeMillis());

         MavenGoogleDriveDownloadFeedbackOutputStream feedbackOutput = new MavenGoogleDriveDownloadFeedbackOutputStream(transferEvent, output, resource.getContentLength());
         googleDrive.downloadFile(downloadGoogleDriveFile, feedbackOutput);
      } catch (IOException ex) {
         throw new IOException("Could not read from Google Drive file: " + gDriveBaseDirFile + "/" + downloadGoogleDriveFile.getName(), ex);
      }
   }

   @Override
   public void fillOutputData(OutputData outputData) throws TransferFailedException {
      // We override put and putFromStream
      throw new UnsupportedOperationException("Google Drive Wagon implementation is required to override put methods");
   }

   @Override
   public void put(java.io.File source, String resourceName) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
      // Upload to GDrive
      // Cannot use standard input / output, so takeover entire put method
      //  TODO: See if can setup sharing group / users on Gdrive using ...      RepositoryPermissions permissions = repository.getPermissions();

      // ---------------
      Resource resource = new Resource(resourceName);
      firePutInitiated(resource, source);

      resource.setContentLength(source.length());
      resource.setLastModified(source.lastModified());

      if (googleDrive.getRole() == RepositoryRole.REPOSITORY_SUBSCRIBER) {
         TransferFailedException ex = new TransferFailedException("Repository role REPOSITORY_SUBSCRIBER cannot publish. Authorize as REPOSITORY_PUBLISHER or REPOSITORY_OWNER");
         fireTransferError(resource, ex, TransferEvent.REQUEST_PUT);
         throw ex;
      }

      String filePath = resource.getName();
      firePutStarted(resource, source);

      try (FileInputStream fis = new FileInputStream(source)) {
         upload(resource, fis);
      } catch (IOException ex) {
         fireTransferError(resource, ex, TransferEvent.REQUEST_PUT);
         throw new TransferFailedException("Could not open local file: " + filePath, ex);
      }
      firePutCompleted(resource, source);
   }

   @Override
   protected void putFromStream(InputStream stream, Resource resource) throws TransferFailedException, AuthorizationException, ResourceDoesNotExistException {
      firePutStarted(resource, null);
      upload(resource, stream);
      firePutCompleted(resource, null);
   }

   private void upload(Resource resource, InputStream contentStream) throws TransferFailedException {
      // Note: Snapshot deploys will contain a unique timestamp
      String filePath = resource.getName();
      String fileDir = PathUtils.dirname(resource.getName());
      String file = PathUtils.filename(resource.getName());

      File parentDir;
      Logger.debug(String.format("Uploading file %s[size=%d] to Google Drive file", filePath, resource.getContentLength()));
      try {
         parentDir = googleDrive.createFolder(gDriveBaseDirFile.getId(), fileDir);
         Logger.trace(String.format("Located or created Google Drive folder %s[id=%s] under %s[id=%s]", fileDir, parentDir.getId(), gDriveBaseDir, gDriveBaseDirFile.getId()));
      } catch (IOException ex) {
         fireTransferError(resource, ex, TransferEvent.REQUEST_PUT);
         throw new TransferFailedException("Could not locate or create or obtain Google Drive folder: " + gDriveBaseDirFile + "/" + fileDir, ex);
      }

      // Upload the file (Deletes existing file if present)
      try {
         TransferEvent transferEvent = new TransferEvent(this, resource, TransferEvent.TRANSFER_PROGRESS, TransferEvent.REQUEST_PUT);
         transferEvent.setTimestamp(System.currentTimeMillis());
         MavenGoogleDriveUploadFeedbackInputStream listener = new MavenGoogleDriveUploadFeedbackInputStream(transferEvent, contentStream, (int) resource.getContentLength());

         String mimeType = googleDrive.getMineType(file);
         File uploadedFile = googleDrive.uploadFile(parentDir.getId(), file, mimeType, listener);
      } catch (IOException ex) {
         fireTransferError(resource, ex, TransferEvent.REQUEST_PUT);
         throw new TransferFailedException("Could not upload file Google Drive file: " + gDriveBaseDirFile + "/" + filePath, ex);
      }
   }

   @Override
   protected void openConnectionInternal() throws ConnectionException, AuthenticationException {
      init();
      Logger.debug("Connecting to Google Drive base dir " + gDriveBaseDir
              + ", connection timeout " + (getTimeout() / 1000)
              + "s, read timeout " + (getReadTimeout() / 1000) + "s");

      try {
         googleDrive = new GoogleDrive(getTimeout(), getReadTimeout());
         googleDrive.connect(getRepository().getId(), storedCredential);
         fireSessionLoggedIn();
         Logger.debug(String.format("Connected to Google Drive using repository role %s", googleDrive.getRole()));
      } catch (Exception ex) {
         throw new AuthenticationException("Unable to authorize with Google Drive", ex);
      }

      boolean created = false;
      try {
         // Handle base directory according to the role 
         switch (googleDrive.getRole()) {
            case REPOSITORY_PUBLISHER:
               // First check for a  directory that is shared and writable
               gDriveBaseDirFile = googleDrive.getSharedWithMeFolder(gDriveBaseDir, true);

               // If not shared, then create own one
               if (null == gDriveBaseDirFile) {
                  gDriveBaseDirFile = googleDrive.createRootFolder(gDriveBaseDir);
                  if (null != gDriveBaseDirFile) {
                     created = true;
                  }
               }
               break;

            case REPOSITORY_SUBSCRIBER:
               // The directory must be shared and read-only
               gDriveBaseDirFile = googleDrive.getSharedWithMeFolder(gDriveBaseDir, false);
               break;

            case REPOSITORY_PRIVATE:
               // The directory must exist and be owned in the application space
               gDriveBaseDirFile = googleDrive.createRootFolder(gDriveBaseDir);
               if (null != gDriveBaseDirFile) {
                  created = true;
               }
               break;

         }

         if (null == gDriveBaseDirFile) {
            throw new Exception("Not found");
         }
         if (created) {
            Logger.trace(String.format("Located or created Google Drive root folder %s[id=%s]", gDriveBaseDir, gDriveBaseDirFile.getId()));
            Logger.trace(String.format("Base directory %s", gDriveBaseDir));

         } else {
            Logger.trace(String.format("Base directory %s[id=%s] editable: %s", gDriveBaseDir, gDriveBaseDirFile.getId(), gDriveBaseDirFile.getCapabilities().getCanEdit()));
         }

      } catch (Exception ex) {
         throw new ConnectionException("Unable to create or obtain base dir " + gDriveBaseDir + " on Google Drive", ex);
      }
   }

   @Override
   public void closeConnection() throws ConnectionException {
      if (null != googleDrive) {
         googleDrive.disconnect();
         fireSessionLoggedOff();
         Logger.debug("Disconnected from Google Drive");
      }
   }

   private void init() throws ConnectionException {
      Logger.setLogLevels(debug, trace);

      // Treat the URL as a URI as Google Drive already provides endpoint over network
      try {
         URI uri = new URI(getRepository().getUrl());
         gDriveBaseDir = uri.getPath();
         if (gDriveBaseDir.startsWith("/")) {
            gDriveBaseDir = gDriveBaseDir.substring(1);
         }
         if (gDriveBaseDir.endsWith("/")) {
            gDriveBaseDir = gDriveBaseDir.substring(0, gDriveBaseDir.length() - 1);
         }
      } catch (URISyntaxException ex) {
         throw new ConnectionException("Invalid URI " + getRepository().getUrl() + ", needs to be in form gdrive:[path]", ex);
      }

      if (null == storedCredential || storedCredential.isEmpty()) {
         throw new ConnectionException("Unable to obtain stored credential for server id " + getRepository().getId() + ". Run mvn org.rogueware.mojo:gdrive-release-maven-plugin:1.0-SNAPSHOT:authorize");
      }
   }

   protected void proxyFireTransferProgress(TransferEvent transferEvent, byte[] buffer, int n) {
      fireTransferProgress(transferEvent, buffer, n);
   }

}
