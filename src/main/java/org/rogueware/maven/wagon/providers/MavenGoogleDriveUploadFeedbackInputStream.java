/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven.wagon.providers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.apache.maven.wagon.events.TransferEvent;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MavenGoogleDriveUploadFeedbackInputStream extends InputStream {

   private final TransferEvent transferEvent;
   private final InputStream contentStream;
   private final int size;
   private final ByteBuffer buffer;

   public MavenGoogleDriveUploadFeedbackInputStream(TransferEvent transferEvent, InputStream contentStream, int size) {
      this.transferEvent = transferEvent;
      this.contentStream = contentStream;
      this.size = size;

      // Set the feeback chunk size based on upload size
      if (size > 524280) {
         // > 5MB 
         buffer = ByteBuffer.allocate(20480);  // 20K buffer
      } else if (size > 1048576) {
         // > 1MB
         buffer = ByteBuffer.allocate(5125);  // 5K buffer         
      } else if (size > 262144) {
         // 256KB
         buffer = ByteBuffer.allocate(2048);  // 2K buffer                  
      } else if (size > 51200) {
         // 50KB
         buffer = ByteBuffer.allocate(1024);  // 1K buffer                  
      } else if (size > 10240) {
         // 10KB
         buffer = ByteBuffer.allocate(128);  // 128B buffer                           
      } else if (size > 1024) {
         // 1KB
         buffer = ByteBuffer.allocate(10);  // 10B buffer                           
      } else {
         buffer = ByteBuffer.allocate(5);  // 5B buffer                                    
      }
   }

   public int getSize() {
      return size;
   }

   public TransferEvent getTransferEvent() {
      return transferEvent;
   }

   public void finish() {
      if (buffer.position() > 0) {
         byte[] transferred = Arrays.copyOf(buffer.array(), buffer.position());
         ((GoogleDriveWagon) transferEvent.getWagon()).proxyFireTransferProgress(
                 transferEvent, transferred, transferred.length);
      }
      buffer.rewind();
   }

   @Override
   public int read() throws IOException {
      int b = contentStream.read();

      // Buffer to track download and fire transfer progress event for hash calculations and progress indicator
      if (-1 != b) {
         buffer.put((byte) b);
         if (buffer.remaining() == 0) {
            byte[] transferred = buffer.array();
            ((GoogleDriveWagon) transferEvent.getWagon()).proxyFireTransferProgress(
                    transferEvent, transferred, transferred.length);
            buffer.rewind();
         }
      }

      return b;
   }

   @Override
   public long skip(long n) throws IOException {
      return contentStream.skip(n);
   }

   @Override
   public int available() throws IOException {
      return contentStream.available();
   }

   @Override
   public synchronized void mark(int readlimit) {
      contentStream.mark(readlimit);
   }

   @Override
   public synchronized void reset() throws IOException {
      contentStream.reset();
   }

   @Override
   public boolean markSupported() {
      return contentStream.markSupported();
   }
}
