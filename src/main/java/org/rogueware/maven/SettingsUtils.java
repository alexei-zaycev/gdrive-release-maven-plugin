/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.Xpp3DomBuilder;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class SettingsUtils {

   public static Server getServer(Settings settings, String serverId) {
      for (Server server : settings.getServers()) {
         if (serverId.equals(server.getId())) {
            return server;
         }
      }
      return null;
   }

   public static String getServerStoredClientCredential(Settings settings, String serverId) {
      String storedCredential = null;
      Server server = getServer(settings, serverId);

      if (null != server && null != server.getConfiguration() && server.getConfiguration() instanceof Xpp3Dom) {
         Xpp3Dom dom = (Xpp3Dom) server.getConfiguration();
         storedCredential = dom.getChild("storedCredential") == null
                 ? null
                 : dom.getChild("storedCredential").getValue();
      }
      return storedCredential;
   }

   public static boolean hasServerStoredClientCredential(File settingsFile, String serverId) throws XmlPullParserException, IOException {
      String sc = getServerStoredClientCredential(settingsFile, serverId);
      return null != sc && !sc.isEmpty();
   }

   public static String getServerStoredClientCredential(File settingsFile, String serverId) throws XmlPullParserException, IOException {
      String storedCredential = null;
      try (InputStream is = new FileInputStream(settingsFile)) {
         Xpp3Dom settingsXml = Xpp3DomBuilder.build(is, "UTF-8");

         Xpp3Dom serversXml = settingsXml.getChild("servers");
         if (null == serversXml) {
            return null;
         }

         for (Xpp3Dom serverXml : serversXml.getChildren()) {
            String id = serverXml.getChild("id") == null ? null : serverXml.getChild("id").getValue();
            if (serverId.equals(id)) {
               storedCredential = serverXml.getChild("configuration") == null || serverXml.getChild("configuration").getChild("storedCredential") == null
                       ? null
                       : serverXml.getChild("configuration").getChild("storedCredential").getValue();
               return storedCredential;
            }
         }
      }

      return storedCredential;
   }

   public static void addServerToSettingsFile(File settingsFile, String serverId, String storedCredential) throws FileNotFoundException, IOException, XmlPullParserException {
      Xpp3Dom settingsXml;

      try (InputStream is = new FileInputStream(settingsFile)) {
         settingsXml = Xpp3DomBuilder.build(is, "UTF-8");

         Xpp3Dom serversXml = settingsXml.getChild("servers");
         if (null == serversXml) {
            serversXml = new Xpp3Dom("servers");
            settingsXml.addChild(serversXml);
         }

         // First delete the existing server id if it exists
         for (int i = 0; i < serversXml.getChildCount(); i++) {
            Xpp3Dom serverXml = serversXml.getChild(i);
            String id = serverXml.getChild("id") == null ? null : serverXml.getChild("id").getValue();
            if (serverId.equals(id)) {
               serversXml.removeChild(i);
               break;
            }
         }

         // Create the new server entry  
         Xpp3Dom timeoutXml = new Xpp3Dom("timeout");
         timeoutXml.setValue("90000");

         Xpp3Dom readTimeoutXml = new Xpp3Dom("readTimeout");
         readTimeoutXml.setValue("300000");

         Xpp3Dom storedCredentialXml = new Xpp3Dom("storedCredential");
         storedCredentialXml.setValue(storedCredential);

         Xpp3Dom configXml = new Xpp3Dom("configuration");
         configXml.addChild(timeoutXml);
         configXml.addChild(readTimeoutXml);
         configXml.addChild(storedCredentialXml);

         Xpp3Dom idXml = new Xpp3Dom("id");
         idXml.setValue(serverId);

         Xpp3Dom serverXml = new Xpp3Dom("server");
         serverXml.addChild(idXml);
         serverXml.addChild(configXml);

         serversXml.addChild(serverXml);
      }

      // Write the file
      try (FileWriter fw = new FileWriter(settingsFile, false);) {
         fw.write(settingsXml.toString());
      }
   }

}
