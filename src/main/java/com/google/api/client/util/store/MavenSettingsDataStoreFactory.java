/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package com.google.api.client.util.store;

import com.google.api.client.util.Base64;
import com.google.api.client.util.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MavenSettingsDataStoreFactory extends AbstractDataStoreFactory {

   private final String serverId;

   private HashMap<String, byte[]> valueMap;
   private RepositoryRole role;

   public MavenSettingsDataStoreFactory(String serverId, String storedCredentialString) throws IOException {
      this.serverId = serverId;
      load(serverId, storedCredentialString);
   }

   public RepositoryRole getRole() {
      return role;
   }

   @Override
   protected <V extends Serializable> DataStore<V> createDataStore(String id) throws IOException {
      return new MavenSettingsDataStore<>(this, id, serverId, valueMap);
   }

   private void load(String serverId, String storedCredentialStr) throws IOException {
      try {
         if (null == storedCredentialStr || storedCredentialStr.isEmpty()) {
            throw new IOException("Unable to obtain stored client setting for server id " + serverId + " in settings file. Run mvn org.rogueware.mojo:gdrive-release-maven-plugin:authorize");
         }

         byte[] rawStoredClient = Base64.decodeBase64(storedCredentialStr);
         ByteArrayInputStream byteStream = new ByteArrayInputStream(rawStoredClient);

         int totalBytes = 0;
         byte[] serializedBytes = new byte[rawStoredClient.length * 10];
         try (GZIPInputStream zipStream = new GZIPInputStream(byteStream)) {
            int numBytes;
            while ((numBytes = zipStream.read(serializedBytes, totalBytes, serializedBytes.length - totalBytes)) != -1) {
               totalBytes += numBytes;
            }
         }

         StoredCredential storedCredential = IOUtils.deserialize(Arrays.copyOfRange(serializedBytes, 0, totalBytes));
         valueMap = storedCredential.getKeyValueMap();
         role = storedCredential.getRole();
      } catch (IOException ex) {
         throw ex;
      } catch (Exception ex) {
         throw new IOException("Unable to obtain stored client for Google Drive authorization", ex);
      }
   }

   static class MavenSettingsDataStore<V extends Serializable> extends AbstractMemoryDataStore<V> {

      MavenSettingsDataStore(MavenSettingsDataStoreFactory dataStore, String id, String serverId, HashMap<String, byte[]> valueMap) throws IOException {
         super(dataStore, id);

         keyValueMap = valueMap;
      }

      @Override
      void save() throws IOException {
         // We never resave even if token was refreshed as next time a new token will be refreshed anyway
      }

      @Override
      public MavenSettingsDataStoreFactory getDataStoreFactory() {
         return (MavenSettingsDataStoreFactory) super.getDataStoreFactory();
      }

   }
}
