/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package com.google.api.client.util.store;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class StoredCredential implements Serializable {

   private RepositoryRole role;
   private HashMap<String, byte[]> keyValueMap;

   public StoredCredential(RepositoryRole role, HashMap<String, byte[]> keyValueMap) {
      this.role = role;
      this.keyValueMap = keyValueMap;
   }

   public RepositoryRole getRole() {
      return role;
   }

   public HashMap<String, byte[]> getKeyValueMap() {
      return keyValueMap;
   }
}
