/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package com.google.api.client.util.store;

import com.google.api.client.util.Base64;
import com.google.api.client.util.IOUtils;
import com.google.api.client.util.Maps;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class Base64DataStoreFactory extends AbstractDataStoreFactory {
   
   private String storedCredentialString;
   private final RepositoryRole role;
   
   public Base64DataStoreFactory(RepositoryRole role) {
      this.role = role;
   }
   
   @Override
   protected <V extends Serializable> DataStore<V> createDataStore(String id) throws IOException {
      return new Base64DataStore<>(this, id, role);
   }
   
   public String getStoredCredentialString() {
      return storedCredentialString;
   }
   
   class Base64DataStore<V extends Serializable> extends AbstractMemoryDataStore<V> {
      
      RepositoryRole role;
      
      Base64DataStore(Base64DataStoreFactory dataStore, String id, RepositoryRole role) throws IOException {
         super(dataStore, id);
         this.role = role;
         keyValueMap = Maps.newHashMap();
      }
      
      @Override
      void save() throws IOException {         
         byte[] storedCredential = IOUtils.serialize(new StoredCredential(role, keyValueMap));
         
         ByteArrayOutputStream byteStream = new ByteArrayOutputStream(storedCredential.length);
         try (GZIPOutputStream zipStream = new GZIPOutputStream(byteStream)) {
            zipStream.write(storedCredential);
         }
         
         byte[] compressedStoredCredentidal = byteStream.toByteArray();
         storedCredentialString = Base64.encodeBase64String(compressedStoredCredentidal).trim();
      }
      
      @Override
      public FileDataStoreFactory getDataStoreFactory() {
         return (FileDataStoreFactory) super.getDataStoreFactory();
      }
   }
}
