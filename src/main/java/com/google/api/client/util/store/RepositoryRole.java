/*
 * Copyright 2017 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package com.google.api.client.util.store;

import com.google.api.services.drive.DriveScopes;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public enum RepositoryRole {
   REPOSITORY_PUBLISHER(DriveScopes.DRIVE), // Maven repository folder is shared or owned by the owner to the user and has rw
   REPOSITORY_SUBSCRIBER(DriveScopes.DRIVE_READONLY), // Maven repository folder is shared by the owner to the user and has ro
   REPOSITORY_PRIVATE(DriveScopes.DRIVE_FILE); // Maven repository folder is owned by the user in app space and has rw (private repository)

   private final String driveScope;

   private RepositoryRole(String driveScope) {
      this.driveScope = driveScope;
   }

   public String getDriveScope() {
      return driveScope;
   }
}
